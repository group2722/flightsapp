﻿using Domain.Enums;
using MediatR;

namespace Application.Flight.Commands
{
    public class ChangeStatusCommand : IRequest
    {
        public Guid Id { get; set; }
        public FlightStatus Status { get; set; }
    }
}
