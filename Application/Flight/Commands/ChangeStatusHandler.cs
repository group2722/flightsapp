﻿using Application.Services.Interfaces;
using MediatR;
using FlightEntity = Domain.Entities.Flight;

namespace Application.Flight.Commands
{
    public class ChangeStatusHandler : IRequestHandler<ChangeStatusCommand>
    {
        private readonly IRepository<FlightEntity> _flightsRepository;

        public ChangeStatusHandler(IRepository<FlightEntity> flightsRepository)
        {
            _flightsRepository = flightsRepository;
        }

        public async Task<Unit> Handle(ChangeStatusCommand request, CancellationToken cancellationToken)
        {
            var flight = await _flightsRepository.GetByIdAsync(request.Id);

            if (flight != null)
            {
                flight.Status = request.Status;
                await _flightsRepository.UpdateAsync(flight);
            }
            //else 404

            return Unit.Value;
        }
    }
}
