﻿using Domain.Enums;
using MediatR;
using FlightEntity = Domain.Entities.Flight;

namespace Application.Flight.Commands
{
    public class CreateFlightCommand : IRequest<FlightEntity>
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTimeOffset Departure { get; set; }
        public DateTimeOffset Arrival { get; set; }
        public FlightStatus Status { get; set; }
    }
}
