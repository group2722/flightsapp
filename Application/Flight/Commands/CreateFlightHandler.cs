﻿using Application.Services.Interfaces;
using AutoMapper;
using MediatR;
using FlightEntity = Domain.Entities.Flight;

namespace Application.Flight.Commands
{
    public class CreateFlightHandler : IRequestHandler<CreateFlightCommand, FlightEntity>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<FlightEntity> _flightsRepository;

        public CreateFlightHandler(IMapper mapper, IRepository<FlightEntity> flightsRepository)
        {
            _mapper = mapper;
            _flightsRepository = flightsRepository;
        }

        public async Task<FlightEntity> Handle(CreateFlightCommand request, CancellationToken cancellationToken)
        {
            var flight = _mapper.Map<FlightEntity>(request);
            var createdFlight = await _flightsRepository.CreateAsync(flight);
            return createdFlight;
        }
    }
}
