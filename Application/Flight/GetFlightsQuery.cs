﻿using MediatR;
using Application.Services.Interfaces;
using FlightEntity = Domain.Entities.Flight;

namespace Application.Flight
{
    public class GetFlightsQuery : IRequest<IEnumerable<FlightEntity>>
    {
    }
}
