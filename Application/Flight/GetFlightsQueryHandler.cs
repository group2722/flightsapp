﻿using Application.Services.Interfaces;
using MediatR;
using FlightEntity = Domain.Entities.Flight;

namespace Application.Flight
{
    public class GetFlightsQueryHandler : IRequestHandler<GetFlightsQuery, IEnumerable<FlightEntity>>
    {
        private readonly IRepository<FlightEntity> _flightsRepository;

        public GetFlightsQueryHandler(IRepository<FlightEntity> flightsRepository)
        {
            _flightsRepository = flightsRepository;
        }

        public async Task<IEnumerable<FlightEntity>> Handle(GetFlightsQuery request, CancellationToken cancellationToken)
        {
            var flights = await _flightsRepository.GetAllAsync();
            return flights.OrderBy(x => x.Arrival);
        }
    }
}
