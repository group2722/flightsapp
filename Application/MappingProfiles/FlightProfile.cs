﻿using Application.Flight.Commands;
using AutoMapper;


namespace Application.MappingProfiles
{
    public class FlightProfile : Profile
    {
        public FlightProfile()
        {
            CreateMap<CreateFlightCommand, Domain.Entities.Flight>();
        }
    }
}
