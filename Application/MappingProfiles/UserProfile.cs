﻿using Application.User.Registration;
using AutoMapper;

namespace Application.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<RegistrationCommand, Domain.Entities.User>();
        }
    }
}
