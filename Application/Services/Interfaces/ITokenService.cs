﻿namespace Application.Services.Interfaces
{
    public interface ITokenService
    {
        string GetToken(string username, string password);
    }
}
