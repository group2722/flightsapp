﻿using MediatR;

namespace Application.User.Authorization
{
    public class AuthorizationCommand : IRequest<AuthorizationResponse>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
