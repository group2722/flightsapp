﻿using Application.Services.Interfaces;
using MediatR;
using UserEntity = Domain.Entities.User;

namespace Application.User.Authorization
{
    public class AuthorizationHandler : IRequestHandler<AuthorizationCommand, AuthorizationResponse>
    {
        private readonly IRepository<UserEntity> _userRepository;
        private readonly ITokenService _tokenService;
        private readonly IPasswordHasher _passwordHasher;

        public AuthorizationHandler(IRepository<UserEntity> userRepository, ITokenService tokenService, IPasswordHasher passwordHasher)
        {
            _userRepository = userRepository;
            _tokenService = tokenService;
            _passwordHasher = passwordHasher;
        }

        public async Task<AuthorizationResponse> Handle(AuthorizationCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FirstOrDefault(x => x.Username == request.Username);

            if (user == null)
                throw new Exception("Invalid username");

            if (!_passwordHasher.VerifyHashedPassword(request.Password, user.Password))
                throw new Exception("Invalid password");

            var token = _tokenService.GetToken(user.Username, user.Password);

            return new AuthorizationResponse()
            {
                Username = user.Username,
                Token = token
            };
        }
    }
}
