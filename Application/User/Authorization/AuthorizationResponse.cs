﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.User.Authorization
{
    public class AuthorizationResponse
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
