﻿using MediatR;
using UserEntity = Domain.Entities.User;

namespace Application.User.Registration
{
    public class RegistrationCommand : IRequest<UserEntity>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
