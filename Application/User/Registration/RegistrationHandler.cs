﻿using AutoMapper;
using UserEntity = Domain.Entities.User;
using MediatR;
using Application.Services.Interfaces;

namespace Application.User.Registration
{
    public class RegistrationHandler : IRequestHandler<RegistrationCommand, UserEntity>
    {
        private readonly IRepository<UserEntity> _userRepository;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher _passwordHasher;

        public RegistrationHandler(IRepository<UserEntity> userRepository, IMapper mapper, IPasswordHasher passwordHasher)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _passwordHasher = passwordHasher;
        }

        public async Task<UserEntity> Handle(RegistrationCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FirstOrDefault(x => x.Username == request.Username);
            if (user != null)
                throw new Exception($"Username {request.Username} is already exists");

            user = _mapper.Map<UserEntity>(request);

            var hashedPassword = _passwordHasher.HashPassword(request.Password);
            user.Password = hashedPassword;
            var createdUser = await _userRepository.CreateAsync(user);

            return createdUser;
        }
    }
}
