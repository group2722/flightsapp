﻿using Application.Flight.Commands;
using FluentValidation;

namespace Application.Validators
{
    public class CreateFlightValidator : AbstractValidator<CreateFlightCommand>
    {
        public CreateFlightValidator()
        {
            RuleFor(x => x.Origin).NotEmpty().Length(1, 256);
            RuleFor(x => x.Destination).NotEmpty().Length(1, 256);
            RuleFor(x => x.Departure).NotEmpty();
            RuleFor(x => x.Arrival).NotEmpty();
            RuleFor(x => x.Status).IsInEnum();
        }
    }

    public class ChangeFlightStatusValidator : AbstractValidator<ChangeStatusCommand>
    {
        public ChangeFlightStatusValidator()
        {
            RuleFor(x => x.Id).NotEmpty().NotNull();
            RuleFor(x => x.Status).IsInEnum();
        }
    }
}
