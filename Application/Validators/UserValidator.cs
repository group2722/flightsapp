﻿using Application.User.Authorization;
using Application.User.Registration;
using FluentValidation;

namespace FlightsApi.Models.Users
{
    public class RegisterModelValidator : AbstractValidator<AuthorizationCommand>
    {
        public RegisterModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }

    public class AuthenticateRequestModelValidator : AbstractValidator<RegistrationCommand>
    {
        public AuthenticateRequestModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
