﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DB.Configuration
{
    internal class FlightConfiguration : IEntityTypeConfiguration<Flight>
    {
        public void Configure(EntityTypeBuilder<Flight> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Origin).HasMaxLength(256);
            builder.Property(x => x.Destination).HasMaxLength(256);
            builder.Property(x => x.Status).HasConversion<string>();
        }
    }
}
