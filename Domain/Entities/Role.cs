﻿using System;

namespace Domain.Entities
{
    public class Role : IEntity
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
    }
}
