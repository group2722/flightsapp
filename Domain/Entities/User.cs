﻿using System;

namespace Domain.Entities
{
    public class User : IEntity
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Guid? RoleId { get; set; }
        public Role? Role { get; set; }
    }
}
