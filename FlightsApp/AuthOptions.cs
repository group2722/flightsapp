﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace FlightsApi
{
    public static class AuthOptions
    {
        const string KEY = "acf6f790-1314-4be4-8121-1c31e8bc76bb";   // ключ для шифрации
        public const int LIFETIME = 60; // время жизни токена - 60 минут
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
        }
    }
}
