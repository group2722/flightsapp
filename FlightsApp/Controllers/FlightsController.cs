﻿using Application.Flight;
using Application.Flight.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FlightsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FlightsController : ControllerBase
    {
        private readonly IMediator _mediatr;

        public FlightsController(IMediator mediatr)
        {
            _mediatr = mediatr;
        }


        [HttpGet]
        public async Task<IActionResult> GetAllAsync(GetFlightsQuery request)
        {
            var flights = await _mediatr.Send(request);
            return Ok(flights);
        }

        [Authorize(Roles = "Moderator")]
        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync([FromBody]CreateFlightCommand request)
        {
            var createdFlight = await _mediatr.Send(request);
            return Ok(createdFlight);
        }

        [Authorize(Roles = "Moderator")]
        [HttpPut("changestatus")]
        public async Task<IActionResult> ChangeStatus([FromBody]ChangeStatusCommand request)
        {
            await _mediatr.Send(request);   
            return Ok("Flight's status updated");
        }
    }
}
