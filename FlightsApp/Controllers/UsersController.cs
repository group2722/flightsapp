﻿using Application.User.Authorization;
using Application.User.Registration;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace FlightsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;
        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody]RegistrationCommand request)
        {
            var user = await _mediator.Send(request);
            return Ok(user);
        }

        [HttpPost("authorize")]
        public async Task<IActionResult> AuthorizeAsync([FromBody]AuthorizationCommand request)
        {
            var response = await _mediator.Send(request);
            return Ok(response);
        }
    }
}
