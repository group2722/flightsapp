﻿using DB.Data;
using Domain.Entities;
using Domain.Enums;

namespace FlightsApi
{
    public class DataSeed
    {
        public static void CreateTestData(IServiceCollection services)
        {
            using (var provider = services.BuildServiceProvider())
            {
                try
                {
                    var context = provider.GetRequiredService<DataContext>();

                    if (!context.Roles.Any())
                    {
                        context.Roles.Add(new Role() { Code = "Default" });
                        context.Roles.Add(new Role() { Code = "Moderator" });
                        context.SaveChanges();
                    }

                    if (!context.Users.Any())
                    {
                        var role = context.Roles.FirstOrDefault(x => x.Code == "Moderator");
                        var hash = BCrypt.Net.BCrypt.HashPassword("moderator123");
                        context.Users.Add(new User() { Username = "moderator", Password = hash, Role = role });
                        context.SaveChanges();
                    }

                    if (!context.Flights.Any())
                    {
                        context.Flights.Add(new Flight()
                        {
                            Origin = "Almaty",
                            Destination = "Astana",
                            Departure = DateTimeOffset.Now.AddHours(-2),
                            Arrival = DateTimeOffset.Now.AddHours(4),
                            Status = FlightStatus.InTime
                        });

                        context.Flights.Add(new Flight()
                        {
                            Origin = "Almaty",
                            Destination = "Tashkent",
                            Departure = DateTimeOffset.Now.AddHours(-3),
                            Arrival = DateTimeOffset.Now.AddHours(1),
                            Status = FlightStatus.Delayed
                        });

                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    //create manually in DB
                    //throw;
                }
            }
        }
    }
}
