﻿using Application.MappingProfiles;
using Application.Services.Interfaces;
using FlightsApi.Services;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace FlightsApi.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IPasswordHasher, BCryptPasswordHasher>();

            return services;
        }

        public static IServiceCollection AddAppServices(this IServiceCollection services)
        {
            //Load all automapper profiles
            services.AddAutoMapper(typeof(FlightProfile));

            //Fluent Validation
            services.AddFluentValidation(x =>
            {
                x.RegisterValidatorsFromAssemblyContaining<FlightProfile>();
            });

            //MediatR
            services.AddMediatR(typeof(FlightProfile));

            return services;
        }

        public static IServiceCollection AddAuthenticationServices(this IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            ValidateIssuerSigningKey = true,
                        };
                    });

            return services;
        }
    }
}
