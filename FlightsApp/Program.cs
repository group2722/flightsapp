using DB.Data;
using Microsoft.EntityFrameworkCore;
using FlightsApi.Infrastructure.Extensions;
using FlightsApi;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddAuthenticationServices();
builder.Services.AddAuthorization();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddServices();
builder.Services.AddAppServices();

//EF Core connection to DB
builder.Services
       .AddDbContext<DataContext>(opt => {
           opt.UseNpgsql(builder.Configuration
               .GetConnectionString("DbConnection"));
           //fix error with timestamp
           AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
       });


//Fill DB with test data
DataSeed.CreateTestData(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
