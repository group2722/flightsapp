﻿using Application.Services.Interfaces;
using DB.Data;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace FlightsApi.Services
{
    public class Repository<T> : IRepository<T> 
        where T : class, IEntity
    {
        private readonly DataContext _dataContext;
        public Repository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var all = await _dataContext.Set<T>().ToListAsync();
            return all;
        }

        public async Task<IEnumerable<T>> GetWhereAsync(Expression<Func<T, bool>> predicate)
        {
            var list = await _dataContext.Set<T>().Where(predicate).ToListAsync();
            return list;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext
                .Set<T>()
                .FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }

        public async Task<T> CreateAsync(T entity)
        {
            _dataContext.Add(entity);
            await _dataContext.SaveChangesAsync();

            return entity;
        }

        public async Task UpdateAsync(T entity)
        {
            _dataContext.Set<T>().Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(predicate);
            return entity;
        }
    }
}
