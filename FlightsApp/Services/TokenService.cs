﻿using Application.Services.Interfaces;
using DB.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace FlightsApi.Services
{
    public class TokenService : ITokenService
    {
        private readonly DataContext _dataContext;
        public TokenService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public string GetToken(string username, string password)
        {
            var identity = GetIdentity(username, password);
            if (identity == null)
            {
                throw new Exception("Invalid username or password");
            }

            var jwt = new JwtSecurityToken(
                    notBefore: DateTime.UtcNow,
                    claims: identity.Claims,
                    expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private ClaimsIdentity? GetIdentity(string username, string password)
        {
            var user = _dataContext
                .Users
                .Include(x => x.Role)
                .FirstOrDefault(x => x.Username == username && x.Password == password);
            
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Username),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Code ?? "Default")
                };

                var claimsIdentity = new ClaimsIdentity(
                        claims, "Token", 
                        ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);

                return claimsIdentity;
            }

            // если пользователя не найдено
            return null;
        }
    }
}
